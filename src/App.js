import './App.css';
import { Header, ProjectList } from './components'
import { Container } from 'react-bootstrap';

function App() {
  
  return (
    <>
      <Header/>
      <Container className="mt-3">
          <ProjectList />
      </Container>
    </>
  );
}

export default App;