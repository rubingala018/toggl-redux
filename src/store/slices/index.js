import { combineReducers } from "@reduxjs/toolkit";
import projectReducer from "../../components/Features/Projects/projectSlice";
import sidebarReducer from "../../components/Features/Projects/sidebarSlice";


const rootReducer = combineReducers({
    project:projectReducer,
    sidebar:sidebarReducer
});

export default rootReducer;