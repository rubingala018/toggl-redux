import { configureStore } from "@reduxjs/toolkit";
import rootReducer from "./slices";

const store = configureStore({
    reducer: rootReducer,
    //Define all other store configuration as needed
});

export default store;