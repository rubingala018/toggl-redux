import { v4 as uuidv4 } from 'uuid';

const Helpers = (function () {

  function createProject(name, status, description, price) {
    return {
      id: uuidv4(),
      name, 
      status,
      description,
      price,
      elapsed: 0,
      runningSince: null
    };
  }

  function renderElapsedString(elapsed, runningSince) {
      let totalElapsed = elapsed;
      if(runningSince) {
          totalElapsed += Date.now() - runningSince;
      }
      return millisecondsToHuman(totalElapsed);
  }

  function millisecondsToHuman(ms) {
      const seconds = Math.floor((ms/1000) % 60);
      const minutes = Math.floor((ms/1000/60) % 60);
      const hours = Math.floor((ms/1000/60/60));

      const humanized = [
          pad(hours.toString()),
          pad(minutes.toString()),
          pad(seconds.toString())
      ].join(':');

      return humanized;
  }

  function pad(numberString, size=2){
      let padded = numberString;
      while(padded.length < size) {
          padded = `0${padded}`;
      }
      return padded;
  }

  return {
      renderElapsedString,
      millisecondsToHuman,
      pad,
      createProject
  };

}());

export default Helpers;
