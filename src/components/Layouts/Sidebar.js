import React from 'react'
import { Offcanvas } from 'react-bootstrap'
import { AddProject } from '../Features/Projects/AddProject'
import { useSelector } from 'react-redux'

 export const Sidebar = ({onClose}) => {
  const show = useSelector(state => state.sidebar.show);
  return (
    <Offcanvas show={show} onHide={onClose} placement='end'>
        <Offcanvas.Header closeButton>
          <Offcanvas.Title>Add Project</Offcanvas.Title>
        </Offcanvas.Header>
        <Offcanvas.Body>
          <AddProject />
        </Offcanvas.Body>
    </Offcanvas>
  )
}

