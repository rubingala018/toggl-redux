import React, { useEffect, useState } from 'react'
import { Badge, Card, Col, Stack } from 'react-bootstrap';
import Button from 'react-bootstrap/Button';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPencilSquare, faTrash } from '@fortawesome/free-solid-svg-icons';
import { useDispatch } from 'react-redux';
import { deleteProject, editProject, startTimer, stopTimer } from './projectSlice';
import { TimerActionButton } from './TimerActionButton';
import Helpers from '../../../utils/Helpers';

export const ProjectCard = ({id, name, description, price, status, elapsed, runningSince}) => {
    const [counter, setCounter] = useState(0);
    useEffect(() => {
        const timerInterval = setInterval(() => setCounter(counter + 1), 250);
        return () => {
          clearInterval(timerInterval);
        };
      }, [counter]);
    const dispatch = useDispatch();

    const handleStartTimer = () => {
        dispatch(startTimer({projectId: id}
        ));
    }
    const handleStopTimer = () => {
        dispatch(stopTimer({ projectId:id}
        ));
    }
    const handleEdit = () => {
        dispatch(editProject());
    }
    const handleDelete = () => {
        dispatch(deleteProject({ projectId: id}
        ));
    }
  return (
        <Col md={3}>
            <Card border="info">
            <Card.Header>
                <Stack direction="horizontal">
                <h4 className='me-auto'>{name}</h4>
                <div className="vr" />
                <p className='my-auto ms-2'>${price}</p>
                </Stack>
            </Card.Header>
            <Card.Body>
                <Card.Text>
                <span className='d-block'>{description}</span>
                <span className='text-center d-block h2'>{Helpers.renderElapsedString(elapsed, runningSince)}</span>
                </Card.Text>

                <Stack direction="horizontal" gap={3} className='mt-4'>
                <Badge pill bg="info" size="md">Ongoing</Badge>
                <Button variant="outline-danger" className='ms-auto' size="sm" onClick={handleDelete}><FontAwesomeIcon icon={faTrash}/></Button>
                <Button variant="outline-warning" size="sm" onClick={handleEdit}><FontAwesomeIcon icon={faPencilSquare}/></Button>
                </Stack>
                <TimerActionButton
                    runningSince={runningSince}
                    onStart={handleStartTimer}
                    onStop={handleStopTimer}
                />
            </Card.Body>
            </Card>
        </Col>
  )
}
