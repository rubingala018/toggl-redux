import React from 'react'
import { ProjectCard } from './ProjectCard'
import { useSelector } from 'react-redux'
import { Row } from 'react-bootstrap';

export const ProjectList = () => {
  const projects = useSelector(state => state.project.projects);
  return (
    <Row>
        { projects.map(project => ( 
        <ProjectCard
         id={project.id}
         name={project.name}
         description={project.description}
         price={project.price}
         status={project.status}
         elapsed={project.elapsed}
         runningSince={project.runningSince}
         key={project.id}
         />)
         )}
    </Row>
  )
}
