import React, { useRef } from 'react'
import { Button, Form } from 'react-bootstrap'
import { addProject } from './projectSlice';
import { useDispatch } from 'react-redux';
import  Helpers  from '../../../utils/Helpers';
import { toggle } from './sidebarSlice';

export const AddProject = () => {
  const projectNameRef = useRef('');
  const projectDescriptionRef = useRef('');
  const pricePerHourRef = useRef('');
  const projectStatusRef = useRef('Ongoing');
  const dispatch = useDispatch();

  const handleAdd = () => {
    dispatch(addProject());
}

  const handleSubmit = () => {
    const name = projectNameRef.current.value;
    const description = projectDescriptionRef.current.value;
    const price = pricePerHourRef.current.value;
    const status = projectStatusRef.current.value;

    const project = Helpers.createProject(name, status, description, price);
    dispatch(addProject({ project}));
    dispatch(toggle());
  }
  
  return (
    <Form>
        <Form.Group className="mb-3">
          <Form.Label>Project Name</Form.Label>
          <Form.Control
              ref={ projectNameRef }
              type="text"
              placeholder="Enter project name"
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Project Description</Form.Label>
          <Form.Control
              ref={ projectDescriptionRef }
              type="text"
              placeholder="Enter project description"
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Price Per Hour</Form.Label>
          <Form.Control
              ref={pricePerHourRef }
              type="text"
              placeholder="Enter price per hour"
          />
        </Form.Group>
        <Form.Group className="mb-3">
          <Form.Label>Select Status</Form.Label>
          <Form.Select aria-label="Default select example" ref={projectStatusRef}>
            <option value="ongoing">Ongoing</option>
            <option value="completed">Completed</option>
          </Form.Select>
        </Form.Group>
        <Button variant="primary" onClick={handleSubmit}>
          Add Project
        </Button>
    </Form>
  )
}

