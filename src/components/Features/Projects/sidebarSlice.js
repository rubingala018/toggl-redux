import { createSlice } from "@reduxjs/toolkit";

const sidebarSlice = createSlice({
    name: 'Sidebar',
    initialState: {
        show: false
    },
    reducers: {
        toggle : function(currentState){
            return{...currentState, show: !currentState.show};
        }
    }
});

export const { toggle } = sidebarSlice.actions;

const sidebarReducer = sidebarSlice.reducer;
export default sidebarReducer; 