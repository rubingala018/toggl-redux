import React from "react";
import Button from "react-bootstrap/Button";

export const TimerActionButton = ({ runningSince, onStart, onStop }) => {
  if (runningSince) {
      return (
        <Button variant="outline-danger" size="sm" onClick={ onStop}>
          Stop Timer
        </Button>
      );
    }
    return (
      <Button variant="outline-success" size="sm" onClick={ onStart}>
        Start Timer
      </Button>
  );
}
