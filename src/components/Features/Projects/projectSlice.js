import { createSlice } from "@reduxjs/toolkit";

function manageLocalStorage(projects) {
    localStorage.setItem('projects', JSON.stringify(projects));
}

const projectSlice = createSlice({
    name: 'Projects',
    initialState: {
        projects: JSON.parse(localStorage.getItem('projects')) || [] ,
    },
    reducers: {
        addProject: function(currentState, action) {
            const project = action.payload.project;
            const newState = {...currentState, projects:currentState.projects.concat(project)};
            manageLocalStorage(newState.projects);
            return newState;
        },
        editProject: function(currentState) {
            console.log("edit called");
            return {...currentState};
        },
        deleteProject: function(currentState, action) {
            const projectId = action.payload.projectId;
            const updatedProjects = currentState.projects.filter(project => project.id !== projectId)
            manageLocalStorage(updatedProjects)
            return {...currentState, projects:updatedProjects};
        },
        startTimer: function(currentState, action) {
            const projectId = action.payload.projectId;

            const updatedProjects = currentState.projects.map(project => {
                if(project.id === projectId) {
                    return {...project, runningSince: Date.now() };
                }
                return project;
            });
            manageLocalStorage(updatedProjects);
            return {...currentState, projects:updatedProjects};
        },
        stopTimer: function(currentState, action) {
            const projectId = action.payload.projectId;

            const updatedProjects = currentState.projects.map(project => {
                if(project.id === projectId) {
                    return {...project, runningSince:null, elapsed: project.elapsed + Date.now() - project.runningSince };
                }
                return project;
            });
            manageLocalStorage(updatedProjects);
            return {...currentState, projects:updatedProjects};
        },
    }
});

export const { addProject, editProject, deleteProject, startTimer, stopTimer } = projectSlice.actions;

const projectReducer = projectSlice.reducer;
export default projectReducer; 